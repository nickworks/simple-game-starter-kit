﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameStarterKit {
    class Camera {

        public Vector2 position;
        public Matrix matrix;

        static int halfW;
        static int halfH;

        public Camera(int w, int h) {
            SetScreenSize(w, h);
        }
        public void SetScreenSize(int w, int h) {
            Camera.halfW = w / 2;
            Camera.halfH = h / 2;
        }
        public void Update(GameTime gameTime, Vector2 target) {
            position = target;
            
            matrix = Matrix.CreateTranslation(-position.X, -position.Y, 0);
            matrix *= Matrix.CreateTranslation(halfW, halfH, 0);
        }
    }
}
