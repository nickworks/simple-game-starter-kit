﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace GameStarterKit {
    class Sprite {
        protected Texture2D texture;
        public Vector2 position = Vector2.Zero;
        public Vector2 origin;
        public float scale = 1;
        public float rotation = 0;
        public Rectangle crop;
        public SpriteEffects spriteEffects = SpriteEffects.None;
        public Color color = Color.White;

        public Sprite(Texture2D texture) {
            this.texture = texture;
            this.crop = texture.Bounds;
            this.origin = new Vector2(crop.Width / 2, crop.Height / 2);
        }
        public virtual void Draw(SpriteBatch sb) {
            sb.Draw(texture, position, crop, color, rotation, origin, scale, spriteEffects, 0);
        }

    }
}
