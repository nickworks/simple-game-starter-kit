﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameStarterKit {
    static class GameStateManager {

        public static IServiceProvider Services;
        static GameState state;

        static public void Update(GameTime gameTime) {
            if (state != null) state.Update(gameTime);
        }
        static public void Draw(SpriteBatch sb, GameTime gameTime) {
            if (state != null) state.Draw(sb, gameTime);
        }
        static public bool ReadyToSwitch() {
            if (state == null) return true;
            return state.Ending();
        }
        static public void SwitchToPlay() {
            if (ReadyToSwitch()) state = new GSPlay();
        }
    }
}
