﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameStarterKit {
    class GSPlay : GameState {

        Camera cam;

        public GSPlay() {
            cam = new Camera(800, 480);
        }
        public override void Update(GameTime gameTime) {
            UpdateInput();
            // UPDATE STUFF HERE
            cam.Update(gameTime, Vector2.Zero);
            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch sb, GameTime gameTime) {
            sb.Begin(SpriteSortMode.Deferred, null, null, null, null, null, cam.matrix);
            // DRAW STUFF HERE
            sb.End();
        }
    }
}
