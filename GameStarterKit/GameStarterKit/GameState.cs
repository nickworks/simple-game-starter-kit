﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameStarterKit {
    class GameState {

        protected ContentManager Content;
        private KeyboardState ksCurr;
        private KeyboardState ksPrev;
        protected GamePadState gpsCurr;
        protected GamePadState gpsPrev;

        public GameState() {
            Content = new ContentManager(GameStateManager.Services, "Content");
        }
        public virtual void Update(GameTime gameTime) {
            ksPrev = ksCurr;
            gpsPrev = gpsCurr;
        }
        #region INPUT
        protected void UpdateInput() {
            ksCurr = Keyboard.GetState();
            gpsCurr = GamePad.GetState(PlayerIndex.One);
        }
        public bool ControllerConnected() {
            return gpsCurr.IsConnected;
        }
        public Vector2 GetStickL(bool invert = false) {
            Vector2 v = gpsCurr.ThumbSticks.Left;
            if (invert) v.Y *= -1;
            return v;
        }
        public Vector2 GetStickR(bool invert = false) {
            Vector2 v = gpsCurr.ThumbSticks.Right;
            if (invert) v.Y *= -1;
            return v;
        }
        public float GetTriggerL() {
            return gpsCurr.Triggers.Left;
        }
        public float GetTriggerR() {
            return gpsCurr.Triggers.Right;
        }
        public bool OnPress(Keys? key) {
            if (key == null) return false;
            if (ksCurr == null) return false;
            if (ksPrev == null) return false;
            return ksCurr.IsKeyDown(key??Keys.None) && ksPrev.IsKeyUp(key??Keys.None);
        }
        public bool OnPress(Buttons? bttn) {
            if (bttn == null) return false;
            if (gpsCurr == null) return false;
            if (gpsPrev == null) return false;
            if (!gpsCurr.IsConnected) return false;
            return gpsCurr.IsButtonDown(bttn ?? Buttons.A) && gpsPrev.IsButtonUp(bttn ?? Buttons.A);
        }
        public bool OnPress(Buttons? bttn, Keys? key) {
            return OnPress(bttn) || OnPress(key);
        }
        public bool IsPressed(Keys? key) {
            if (key == null) return false;
            if (ksCurr == null) return false;
            return ksCurr.IsKeyDown(key ?? Keys.None);
        }
        public bool IsPressed(Buttons? bttn) {
            if (bttn == null) return false;
            if (gpsCurr == null) return false;
            if (!gpsCurr.IsConnected) return false;
            return gpsCurr.IsButtonDown(bttn ?? Buttons.A);
        }
        public bool IsPressed(Buttons? bttn, Keys? key) {
            return IsPressed(bttn) || IsPressed(key);
        }
        public bool OnRelease(Keys? key) {
            if (key == null) return false;
            if (ksCurr == null) return false;
            if (ksPrev == null) return false;
            return ksCurr.IsKeyUp(key ?? Keys.None) && ksPrev.IsKeyDown(key ?? Keys.None);
        }
        public bool OnRelease(Buttons? bttn) {
            if (bttn == null) return false;
            if (gpsCurr == null) return false;
            if (gpsPrev == null) return false;
            return gpsCurr.IsButtonUp(bttn ?? Buttons.A) && gpsPrev.IsButtonDown(bttn ?? Buttons.A);
        }
        public bool OnRelease(Buttons? bttn, Keys? key) {
            return OnRelease(bttn) || OnRelease(key);
        }
        #endregion
        public virtual void Draw(SpriteBatch sb, GameTime gameTime) { }
        public virtual bool Ending() {
            return true;
        }
    }
}
