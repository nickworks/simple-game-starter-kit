﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameStarterKit {
    class SpriteSheet : Sprite {
        
        Rectangle[] animFrames = { };
        int animIndex = 0;
        float frameRate = 1f / 10;
        float frameTimer = 0f;

        public SpriteSheet(Texture2D texture)
            : base(texture) {
        }
        public void SetAnim(Rectangle[] frames) {
            if (animFrames != frames) {
                animFrames = frames;
                animIndex = 0;
            }
        }
        public void SetFPS(int frames) {
            frameRate = 1f / frames;
        }
        public virtual void UpdateAnim(GameTime gameTime) {
            // if enough time has passed, switch to next frame

            if (animFrames.Length < 1) return;

            frameTimer += (float) gameTime.ElapsedGameTime.TotalSeconds;
            if (frameTimer >= frameRate) {
                animIndex++;
                frameTimer -= frameRate;
            }
            if (animIndex >= animFrames.Length) animIndex = 0;
            crop = animFrames[animIndex];
        }
        public Rectangle GetRectForFrame(int w, int h, int num) {
            int cols = texture.Width / w;
            int x = (num % cols) * w;
            int y = (num / cols) * h;
            return new Rectangle(x, y, w, h);
        }
        public Rectangle[] MakeAnimSequence(int w, int h, int[] frames) {
            Rectangle[] rects = new Rectangle[frames.Length];

            for (int i = 0; i < rects.Length; i++) {
                rects[i] = GetRectForFrame(w, h, frames[i]);
            }

            return rects;
        }

    }
}
